<?php get_header(); ?>

<div class='body' id='body'>
	<!--
		Tutto il contenuto va qui
	-->
	<div class='first-column-body'>
		<?php
		get_template_part( 'home', 'evidencepost' );
		get_template_part( 'home', 'articles' );
		get_template_part( 'home', 'articlessecond' );
		get_template_part( 'home', 'talentscout' );
		get_template_part( 'home', 'photogallery' );
		get_template_part( 'home', 'disclaimer' );
		?>
		<div class='clearfix'></div>
	</div>
	<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>