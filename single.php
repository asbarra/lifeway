<?php
get_header(); 
?>
<div class="body" id="body">
	<div class='first-column-body'>
		<div class='breadcrumbs'>
			SEI IN: VIAGGI E TURISMO
		</div>
		<div class='post-body'>
	
			<?php while ( have_posts() ) : the_post(); ?>
				<h1 class='title'><?php the_title(); ?></h1>
				<h3 class='subtitle'>Riaprirà a Gerusalemme il più grandel ospedale dei crociati</h3>
				<div class='first-column-post'>
					<div class='share-box'>
						<div class='clearfix'></div>
					</div>
					<div class='related-post-box'>
						<div class='title'>
							FORSE TI PUò INTERESSARE
						</div>
						<div class='item'>
							<div class='img'></div>
							<div class='title'>
								PRIIMO POST CORRELATO
							</div>
							<div class='excerpt'>
								PRIIMO POST CORRELATO PRIIMO POST CORRELATO PRIIMO POST CORRELATO
							</div>
						</div>
						<div class='item'>
							<div class='img'></div>
							<div class='title'>
								PRIIMO POST CORRELATO
							</div>
							<div class='excerpt'>
								PRIIMO POST CORRELATO PRIIMO POST CORRELATO PRIIMO POST CORRELATO
							</div>
						</div>
						<div class='item'>
							<div class='img'></div>
							<div class='title'>
								PRIIMO POST CORRELATO
							</div>
							<div class='excerpt'>
								PRIIMO POST CORRELATO PRIIMO POST CORRELATO PRIIMO POST CORRELATO
							</div>
						</div>
						<div class='item'>
							<div class='title'>
								PRIIMO POST CORRELATO
							</div>
							<div class='excerpt'>
								PRIIMO POST CORRELATO PRIIMO POST CORRELATO PRIIMO POST CORRELATO
							</div>
						</div>
						<div class='item'>
							<div class='title'>
								PRIIMO POST CORRELATO
							</div>
							<div class='excerpt'>
								PRIIMO POST CORRELATO PRIIMO POST CORRELATO PRIIMO POST CORRELATO
							</div>
						</div>
						<div class='clearfix'></div>
					</div>
					<div class='clearfix'></div>
				</div>
				<div class='second-column-post'>
					<?php the_content(); ?>
					<div class='clearfix'></div>
				</div>
			<?php endwhile; // end of the loop. ?>
			<div class='clearfix'></div>
		</div>
		<div class='clearfix'></div>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>