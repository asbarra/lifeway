<div class='talent-scout block'>
	<div class='title'>
		TALENT SCOUT
	</div>
	<div class='photo-button-container'>
		<div class='week-photo'>
			FOTO DELLA SETTIMANA
		</div>
		<div class='month-photo'>
			FOTO DEL MESE
		</div>
		<div class='clearfix'></div>
	</div>
	<div class='photo-slider'></div>
	<div class='your-photos'>
		<div class='title'>
			LE VOSTRE FOTO
		</div>
		<div class='photo'>
			<div class='image'></div>
			<div class='category'>
				ANIMALI
			</div>
		</div>
		<div class='photo'>
			<div class='image'></div>
			<div class='category'>
				VIAGGIO
			</div>
		</div>
		<div class='photo'>
			<div class='image'></div>
			<div class='category'>
				SUBACQUEA
			</div>
		</div>
		<div class='photo'>
			<div class='image'></div>
			<div class='category'>
				PAESAGGIO
			</div>
		</div>
		<div class='photo' style='margin-right:0px'>
			<div class='image'></div>
			<div class='category'>
				CREATIVA
			</div>
		</div>
	</div>
	<div class='clearfix'></div>
</div>