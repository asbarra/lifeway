<div class='articles-second block'>
	
	<?php

	/*
	$args_posts = array('post_type' =>  array('post', 'tribe_events'),
	'meta_query' => array(array('key' => 'riga','value' => 'prima','compare' => 'LIKE' )),'posts_per_page' => 15);
	query_posts($args_posts);
	<?php $category = get_the_category(); echo $category[0]->cat_name;?>
	*/
	$args_posts = array('posts_per_page' => 2);
	query_posts($args_posts);
	$cont=0;
	while (have_posts()) : the_post(); 
	?>
	
	<div class='article'>
		<div class='category'>
			<?php $category = get_the_category(); echo $category[0]->cat_name;?>
		</div>
		<div class='title'>
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		</div>
		<div class='excerpt'>
			<?php //the_excerpt_max_charlength(120); ?>
		</div>
		<div class='image'></div>
	</div>
	
<?php endwhile; wp_reset_query(); // End the loop. Whew. ?>
<div class='clearfix'></div>
</div>