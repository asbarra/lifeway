<div class='second-column-body'>
	<div class='newsletter block'>
		<div class='clearfix'></div>
	</div>
	<div class='social block'>
		<div class='clearfix'></div>
	</div>
	<div class='adv-simil-square block'>
		<div class='image'></div>
		<div class='clearfix'></div>
	</div>
	<div class='adv-boxes block'>
		<div class='image'></div>
		<div class='image'></div>
		<div class='image'></div>
		<div class='clearfix'></div>
	</div>
	<div class='network-articles block'>
		<div class='title'>
			NEWS DAL NETWORK
		</div>
		<div class='article'>
			<div class='title'>
				LA CACCIA DEL GRIZZLY
			</div>
			<div class='excerpt'>
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
			</div>
			<div class='author'>
				by MEEEMEMME
			</div>
			<div class='clearfix'></div>
		</div>
		<div class='article'>
			<div class='title'>
				LA CACCIA DEL GRIZZLY
			</div>
			<div class='excerpt'>
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
			</div>
			<div class='author'>
				by MEEEMEMME
			</div>
			<div class='clearfix'></div>
		</div>
		<div class='article'>
			<div class='title'>
				LA CACCIA DEL GRIZZLY
			</div>
			<div class='excerpt'>
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
			</div>
			<div class='author'>
				by MEEEMEMME
			</div>
			<div class='clearfix'></div>
		</div>
		<div class='article'>
			<div class='title'>
				LA CACCIA DEL GRIZZLY
			</div>
			<div class='excerpt'>
				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
			</div>
			<div class='author'>
				by MEEEMEMME
			</div>
			<div class='clearfix'></div>
		</div>
		<div class='clearfix'></div>
	</div>
	<div class='images-adv block'>
		<div class='image'></div>
		<hr />
		<div class='image'></div>
	</div>
	<div class='disclaimer-contacts block'>
		<ul class='unstyled'>
			<li>Pubblicita</li>
			<li>Contatti</li>
			<li>Feed rss</li>
			<li>Informativa privacy</li>
			<li></li>
			<li>Copyright</li>
			<li style='text-align: right'>Torna su</li>
		</ul>
		<div class='clearfix'></div>
	</div>
	<div class='clearfix'></div>
</div>
<div class='clearfix'></div>