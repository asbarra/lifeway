<?php
get_header(); 
?>
<div class="body" id="body">
	<div class='first-column-body'>
		<div class='breadcrumbs block'>
			SEI IN: VIAGGI E TURISMO
		</div>
		<div class='categories block'>
			<div class='category'>
				<div class='image'></div>
				<div class='text'>
					<div class='title'>
						TEXT
					</div>
					<div class='excerpt'>
						Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
					</div>
				</div>
			</div>
			<div class='clearfix'></div>
			<div class='category'>
				<div class='image'></div>
				<div class='text'>
					<div class='title'>
						TEXT
					</div>
					<div class='excerpt'>
						Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
					</div>
				</div>
			</div>
			<div class='clearfix'></div>
			<div class='category'>
				<div class='image'></div>
				<div class='text'>
					<div class='title'>
						TEXT
					</div>
					<div class='excerpt'>
						Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
					</div>
				</div>
			</div>
			<div class='clearfix'></div>
			<div class='category'>
				<div class='image'></div>
				<div class='text'>
					<div class='title'>
						TEXT
					</div>
					<div class='excerpt'>
						Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
					</div>
				</div>
			</div>
			<div class='clearfix'></div>
		</div>
		<div class='clearfix'></div>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>