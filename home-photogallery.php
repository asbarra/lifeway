<div class='photogallery block'>
	<div class='first-column'>
		<div class='title'>
			PHOTOGALLERY
		</div>
		<div class='big-photo'>
			<div class='title'>
				LE ISOLE PHAER OER
			</div>
			<div class='excerpt'>
				Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id.
			</div>
			<div class='image'></div>
		</div>
		<div class='article-list'>
			<div class='title'>
				ALTRE GALLERY
			</div>
			<div class='item'>
				<div class='image'></div>
				<div class='text'>
					<div class='title'>
						LE ISOLE PHAER OER
					</div>
					<div class='excerpt'>
						bla bla bla bla bla bla bla bla bla bla bla bla
					</div>
				</div>
			</div>
			<div class='item'>
				<div class='image'></div>
				<div class='text'>
					<div class='title'>
						LE ISOLE PHAER OER
					</div>
					<div class='excerpt'>
						bla bla bla bla bla bla bla bla bla bla bla bla
					</div>
				</div>
			</div>
			<div class='item'>
				<div class='image'></div>
				<div class='text'>
					<div class='title'>
						LE ISOLE PHAER OER
					</div>
					<div class='excerpt'>
						bla bla bla bla bla bla bla bla bla bla bla bla
					</div>
				</div>
			</div>
			<div class='item'>
				<div class='image'></div>
				<div class='text'>
					<div class='title'>
						LE ISOLE PHAER OER
					</div>
					<div class='excerpt'>
						bla bla bla bla bla bla bla bla bla bla bla bla
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class='second-column'>
		<div class='item'>
			<div class='category'>
				VIAGGI
			</div>
			<div class='title'>
				IL VELENO DEI SERPENTI
			</div>
			<div class='photo'></div>
		</div>
		<div class='item-adv'></div>
	</div>
	<div class='clearfix'></div>
</div>