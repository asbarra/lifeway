<?php
get_header(); 
?>
<div class="body" id="body">
	<div class='first-column-body'>
		<div class='breadcrumbs block'>
			SEI IN: VIAGGI E TURISMO
		</div>
        <div class='articles block'>
          <div class='article'>
            <div class='category'>
              VIAGGI
            </div>
            <div class='title'>
              IL VELENO DEI SERPENTI
            </div>
            <div class='excerpt'>
              Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
            <div class='image'></div>
          </div>
          <div class='article'>
            <div class='articles-half'>
              <div class='article-half'>
                <div class='category'>
                  VIAGGI
                </div>
                <div class='title'>
                  IL VELENO DEI SERPENTI
                </div>
                <div class='image'></div>
              </div>
              <div class='article-half'>
                <div class='category'>
                  VIAGGI
                </div>
                <div class='title'>
                  IL VELENO DEI SERPENTI
                </div>
                <div class='image'></div>
              </div>
            </div>
            <div class='articles-half'>
              <div class='article-half'>
                <div class='category'>
                  VIAGGI
                </div>
                <div class='title'>
                  IL VELENO DEI SERPENTI
                </div>
                <div class='image'></div>
              </div>
              <div class='article-half'>
                <div class='category'>
                  VIAGGI
                </div>
                <div class='title'>
                  IL VELENO DEI SERPENTI
                </div>
                <div class='image'></div>
              </div>
            </div>
          </div>
          <div class='clearfix'></div>
        </div>
        <div class='articles-second block'>
          <div class='article'>
            <div class='category'>
              VIAGGI
            </div>
            <div class='title'>
              IL VELENO DEI SERPENTI
            </div>
            <div class='excerpt'>
              Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
            <div class='image'></div>
          </div>
          <div class='article'>
            <div class='category'>
              VIAGGI
            </div>
            <div class='title'>
              IL VELENO DEI SERPENTI
            </div>
            <div class='excerpt'>
              Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
            <div class='image'></div>
          </div>
          <div class='clearfix'></div>
        </div>
        <div class='articles-third block'>
          <div class='article'>
            <div class='articles-half'>
              <div class='article-half'>
                <div class='category'>
                  VIAGGI
                </div>
                <div class='title'>
                  IL VELENO DEI SERPENTI
                </div>
                <div class='image'></div>
              </div>
              <div class='article-half'>
                <div class='category'>
                  VIAGGI
                </div>
                <div class='title'>
                  IL VELENO DEI SERPENTI
                </div>
                <div class='image'></div>
              </div>
            </div>
            <div class='articles-half'>
              <div class='article-half'>
                <div class='category'>
                  VIAGGI
                </div>
                <div class='title'>
                  IL VELENO DEI SERPENTI
                </div>
                <div class='image'></div>
              </div>
              <div class='article-half'>
                <div class='category'>
                  VIAGGI
                </div>
                <div class='title'>
                  IL VELENO DEI SERPENTI
                </div>
                <div class='image'></div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
          <div class='article'>
            <div class='articles-half'>
              <div class='article-half'>
                <div class='category'>
                  VIAGGI
                </div>
                <div class='title'>
                  IL VELENO DEI SERPENTI
                </div>
                <div class='image'></div>
              </div>
              <div class='article-half'>
                <div class='category'>
                  VIAGGI
                </div>
                <div class='title'>
                  IL VELENO DEI SERPENTI
                </div>
                <div class='image'></div>
              </div>
            </div>
            <div class='articles-half'>
              <div class='article-half-list'>
                <div class='category'>
                  VIAGGI
                </div>
                <div class='item'>
                  <div class='title'>
                    IL VELENO DEI SERPENTI
                  </div>
                  <div class='excerpt'>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore.
                  </div>
                </div>
                <div class='item'>
                  <div class='title'>
                    IL VELENO DEI SERPENTI
                  </div>
                  <div class='excerpt'>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore.
                  </div>
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>
		<div class='clearfix'></div>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>