<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>
<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> » Blog Archive <?php } ?> <?php wp_title(); ?></title>
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" /> <!-- leave this for stats -->
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri() . "/css/lifeway.css"; ?>" />
	<!-- <?php bloginfo('stylesheet_directory'); ?>/ -->
	<script language="javascript" type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js.js"></script>
	<script language="javascript" type="text/javascript">    
	if(f)
	{
		write_css('<?php bloginfo('stylesheet_directory'); ?>');
	}
	</script>
	<?php wp_head(); ?>
</head>
<body>
	<div class='lifeway-container'>
		<div class='header' id='header'>
			<div class='banner-container'>
				<!--
					Qui ci metto i banner di pubblicita
				-->
				<div class='clearfix'></div>
			</div>
			<div class='network-menu'>
				<!--
					Qui ci metto il menu del network
				-->
				<div class='login'>
					<div class='login-button'>
						<a>
							ENTRA
						</a>
					</div>
					<div class='login-button'>
						<a>
							REGISTRATI
						</a>
					</div>
				</div>
				<div class='network-menu-list'>
					<div class='network-menu-item'>
						<a>
							OASIS
						</a>
					</div>
					<div class='network-menu-item'>
						<a>
							OASIS PHOTOCONTEST
						</a>
					</div>
					<div class='network-menu-item'>
						<a>
							PHOTOFARM
						</a>
					</div>
					<div class='network-menu-item'>
						<a>
							LA VENTA
						</a>
					</div>
					<div class='network-menu-item'>
						<a>
							SUPERSCUBA
						</a>
					</div>
				</div>
			</div>
			<img class='logo' src='img/logo_lifeway.png'>
			<div class='clearfix'></div>
		</img>
		<div class='site-menu'>
			<!--
				Qui ci metto il menu del sito
			-->
			<div class='site-menu-item'>
				<img src='img/logo_lifeway.png' />
			</div>
			<div class='site-menu-item'>
				<a>
					VIAGGI E TURISMO
				</a>
			</div>
			<div class='site-menu-item'>
				<a>
					SCIENZA
				</a>
			</div>
			<div class='site-menu-item'>
				<a>
					FOTOGRAFIA
				</a>
			</div>
			<div class='site-menu-item'>
				<a>
					MY WAY
				</a>
			</div>
			<div class='site-menu-item'>
				<a>
					SHOP
				</a>
			</div>
			<div class='clearfix'></div>
		</div>
		<div class='banner-container-little'>
			<!--
				Qui ci metto i banner di pubblicita
			-->
			<div class='clearfix'></div>
		</div>
	</div>